﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Microsoft.ServiceModel.Samples
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="ERP")]
	public partial class ERPDataModelDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Definiciones de métodos de extensibilidad
    partial void OnCreated();
    #endregion
		
		public ERPDataModelDataContext() : 
				base(global::ERPService.Properties.Settings.Default.ERPConnectionString1, mappingSource)
		{
			OnCreated();
		}
		
		public ERPDataModelDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public ERPDataModelDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public ERPDataModelDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public ERPDataModelDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.VerProveedores")]
		public ISingleResult<VerProveedoresResult> VerProveedores()
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
			return ((ISingleResult<VerProveedoresResult>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.Autenticar")]
		public int Autenticar([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(20)")] string usuario, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(100)")] string clave)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), usuario, clave);
			return ((int)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.ValidarAcceso")]
		public int ValidarAcceso([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NChar(75)")] string sistema, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NChar(75)")] string vista, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NChar(75)")] string recurso, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NChar(20)")] string usuario)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), sistema, vista, recurso, usuario);
			return ((int)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.AccesosPorUsuario")]
		public ISingleResult<AccesosPorUsuarioResult> AccesosPorUsuario([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NChar(75)")] string sistema, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NChar(75)")] string vista, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NChar(20)")] string usuario)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), sistema, vista, usuario);
			return ((ISingleResult<AccesosPorUsuarioResult>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.ListadoPaises")]
		public ISingleResult<ListadoPaisesResult> ListadoPaises()
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
			return ((ISingleResult<ListadoPaisesResult>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.InsertarProveedor")]
		public int InsertarProveedor(
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(45)")] string nombre, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(60)")] string direccion, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(25)")] string telefono, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(40)")] string fax, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(45)")] string correo, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(45)")] string nombre_contacto, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(25)")] string telefono_contacto, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(45)")] string correo_contacto, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")] System.Nullable<int> dias_credito, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Int")] System.Nullable<int> dias_entrega, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(40)")] string pais_origen, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(35)")] string nacionalidad, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(25)")] string nit, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(25)")] string registro, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Decimal(10,2)")] System.Nullable<decimal> margen_usual, 
					[global::System.Data.Linq.Mapping.ParameterAttribute(DbType="Decimal(10,2)")] System.Nullable<decimal> descuento_aplicable)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), nombre, direccion, telefono, fax, correo, nombre_contacto, telefono_contacto, correo_contacto, dias_credito, dias_entrega, pais_origen, nacionalidad, nit, registro, margen_usual, descuento_aplicable);
			return ((int)(result.ReturnValue));
		}
	}
	
	public partial class VerProveedoresResult
	{
		
		private int _id;
		
		private string _nombre;
		
		private string _direccion;
		
		private string _telefono;
		
		private string _fax;
		
		private string _correo;
		
		private string _nombre_contacto;
		
		private string _telefono_contacto;
		
		private string _correo_contacto;
		
		private System.Nullable<int> _dias_credito;
		
		private System.Nullable<int> _dias_entrega;
		
		private string _pais_origen;
		
		private string _nacionalidad;
		
		private string _nit;
		
		private string _registro;
		
		private System.Nullable<decimal> _margen_usual;
		
		private System.Nullable<decimal> _descuento_aplicable;
		
		private int _activo;
		
		public VerProveedoresResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", DbType="Int NOT NULL")]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this._id = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre", DbType="NVarChar(45)")]
		public string nombre
		{
			get
			{
				return this._nombre;
			}
			set
			{
				if ((this._nombre != value))
				{
					this._nombre = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_direccion", DbType="NVarChar(60)")]
		public string direccion
		{
			get
			{
				return this._direccion;
			}
			set
			{
				if ((this._direccion != value))
				{
					this._direccion = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_telefono", DbType="NVarChar(25)")]
		public string telefono
		{
			get
			{
				return this._telefono;
			}
			set
			{
				if ((this._telefono != value))
				{
					this._telefono = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fax", DbType="NVarChar(40)")]
		public string fax
		{
			get
			{
				return this._fax;
			}
			set
			{
				if ((this._fax != value))
				{
					this._fax = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_correo", DbType="NVarChar(45)")]
		public string correo
		{
			get
			{
				return this._correo;
			}
			set
			{
				if ((this._correo != value))
				{
					this._correo = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre_contacto", DbType="NVarChar(45)")]
		public string nombre_contacto
		{
			get
			{
				return this._nombre_contacto;
			}
			set
			{
				if ((this._nombre_contacto != value))
				{
					this._nombre_contacto = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_telefono_contacto", DbType="NVarChar(25)")]
		public string telefono_contacto
		{
			get
			{
				return this._telefono_contacto;
			}
			set
			{
				if ((this._telefono_contacto != value))
				{
					this._telefono_contacto = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_correo_contacto", DbType="NVarChar(45)")]
		public string correo_contacto
		{
			get
			{
				return this._correo_contacto;
			}
			set
			{
				if ((this._correo_contacto != value))
				{
					this._correo_contacto = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dias_credito", DbType="Int")]
		public System.Nullable<int> dias_credito
		{
			get
			{
				return this._dias_credito;
			}
			set
			{
				if ((this._dias_credito != value))
				{
					this._dias_credito = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dias_entrega", DbType="Int")]
		public System.Nullable<int> dias_entrega
		{
			get
			{
				return this._dias_entrega;
			}
			set
			{
				if ((this._dias_entrega != value))
				{
					this._dias_entrega = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_pais_origen", DbType="NVarChar(40)")]
		public string pais_origen
		{
			get
			{
				return this._pais_origen;
			}
			set
			{
				if ((this._pais_origen != value))
				{
					this._pais_origen = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nacionalidad", DbType="NVarChar(35)")]
		public string nacionalidad
		{
			get
			{
				return this._nacionalidad;
			}
			set
			{
				if ((this._nacionalidad != value))
				{
					this._nacionalidad = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nit", DbType="NVarChar(25)")]
		public string nit
		{
			get
			{
				return this._nit;
			}
			set
			{
				if ((this._nit != value))
				{
					this._nit = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_registro", DbType="NVarChar(25)")]
		public string registro
		{
			get
			{
				return this._registro;
			}
			set
			{
				if ((this._registro != value))
				{
					this._registro = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_margen_usual", DbType="Decimal(10,2)")]
		public System.Nullable<decimal> margen_usual
		{
			get
			{
				return this._margen_usual;
			}
			set
			{
				if ((this._margen_usual != value))
				{
					this._margen_usual = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_descuento_aplicable", DbType="Decimal(10,2)")]
		public System.Nullable<decimal> descuento_aplicable
		{
			get
			{
				return this._descuento_aplicable;
			}
			set
			{
				if ((this._descuento_aplicable != value))
				{
					this._descuento_aplicable = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_activo", DbType="Int NOT NULL")]
		public int activo
		{
			get
			{
				return this._activo;
			}
			set
			{
				if ((this._activo != value))
				{
					this._activo = value;
				}
			}
		}
	}
	
	public partial class AccesosPorUsuarioResult
	{
		
		private int _id;
		
		private string _usuario;
		
		private string _sistema;
		
		private string _vista;
		
		private string _recurso;
		
		private string _titulo;
		
		public AccesosPorUsuarioResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", DbType="Int NOT NULL")]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this._id = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_usuario", DbType="NChar(20) NOT NULL", CanBeNull=false)]
		public string usuario
		{
			get
			{
				return this._usuario;
			}
			set
			{
				if ((this._usuario != value))
				{
					this._usuario = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_sistema", DbType="NChar(75) NOT NULL", CanBeNull=false)]
		public string sistema
		{
			get
			{
				return this._sistema;
			}
			set
			{
				if ((this._sistema != value))
				{
					this._sistema = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_vista", DbType="NChar(75) NOT NULL", CanBeNull=false)]
		public string vista
		{
			get
			{
				return this._vista;
			}
			set
			{
				if ((this._vista != value))
				{
					this._vista = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_recurso", DbType="NChar(75) NOT NULL", CanBeNull=false)]
		public string recurso
		{
			get
			{
				return this._recurso;
			}
			set
			{
				if ((this._recurso != value))
				{
					this._recurso = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_titulo", DbType="NChar(200)")]
		public string titulo
		{
			get
			{
				return this._titulo;
			}
			set
			{
				if ((this._titulo != value))
				{
					this._titulo = value;
				}
			}
		}
	}
	
	public partial class ListadoPaisesResult
	{
		
		private short _id;
		
		private string _nombre;
		
		private string _region;
		
		public ListadoPaisesResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", DbType="SmallInt NOT NULL")]
		public short id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this._id = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_nombre", DbType="NChar(20)")]
		public string nombre
		{
			get
			{
				return this._nombre;
			}
			set
			{
				if ((this._nombre != value))
				{
					this._nombre = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_region", DbType="NChar(20)")]
		public string region
		{
			get
			{
				return this._region;
			}
			set
			{
				if ((this._region != value))
				{
					this._region = value;
				}
			}
		}
	}
}
#pragma warning restore 1591
