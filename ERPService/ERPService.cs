﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.ServiceModel;
using ERPService.Models;
using System.Globalization;

namespace Microsoft.ServiceModel.Samples
{
    [ServiceBehavior]
    class ERPService: IERP
    {
        #region List

        public string ObtenerListadoProveedores()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.VerProveedores();
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ListadoPaises()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.ListadoPaises();
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string VerCategorias()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {

                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.VerCategorias();
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerBodegas(int tipo_vista)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.VerBodegas(tipo_vista);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerEmpleados()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.VerEmpleados();
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;

        }

        public string ListadoCatalogos()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    string format = "dd/MM/yyyy";
                    List<object> Data = new List<object>();
                    var Results = db.VerCatalogos(null);

                    foreach (VerCatalogosResult res in Results)
                    {
                        Data.Add(new
                        {
                            res.id,
                            res.nombre,
                            res.descripcion,
                            inicio = ((DateTime)res.inicio).ToString(format),
                            final = ((DateTime)res.final).ToString(format)
                        });
                    }

                    Response = serializer.Serialize(Data);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerDocumentosSinAplicar(string username)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    string format = "dd/MM/yyyy";
                    List<object> Data = new List<object>();

                    var Results = db.VerDocumentosSinAplicar(username);

                    foreach (VerDocumentosSinAplicarResult res in Results)
                    {
                        Data.Add(new
                        {
                            res.id_documento,
                            fecha_creacion = ((DateTime)res.fecha_creacion).ToString(format),
                            res.propietario
                        });
                    }

                    serializer = new JavaScriptSerializer();
                    Response = serializer.Serialize(Data);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerTipoTransacciones()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.VerTipoTransacciones();
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerHistorialTransacciones()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    string format = "dd/MM/yyyy";
                    List<object> Data = new List<object>();

                    var Results = db.VerHistorialTraslados(0, 1000);

                    foreach (VerHistorialTrasladosResult res in Results)
                    {
                        Data.Add(new
                        {
                            fecha = ((DateTime)res.fecha).ToString(format),
                            res.id,
                            res.total_costo,
                            res.total_pares,
                            res.cod,
                            res.transaccion,
                            res.usuario,
                            res.concepto,
                            res.editable
                        });
                    }

                    Response = serializer.Serialize(Data);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string VerTraslado(int id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.VerTraslado(id);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string VerTrasladoDetalle(int id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.VerTrasladoDetalle(id);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string CategoriasDeSeries()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = from st in db.seriestipo select st;
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;

        }

        public string ListadoBancos()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var bancos = from b in db.bancos select b;
                    Response = serializer.Serialize(bancos);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ListadoTipoSolicitudCredito()
        {

            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var creditoTipoSolicitud = from cts in db.credito_tipo_solicitud select cts;
                    Response = serializer.Serialize(creditoTipoSolicitud);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ListadoTipoDescuento()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var tipoDescuento = from td in db.tipodescuento select td;
                    Response = serializer.Serialize(tipoDescuento);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ListadoDescuentos()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var descuento = from d in db.descuento where d.saldo > 0 select d;
                    Response = serializer.Serialize(descuento);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ListadoDescuentosAplicados()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var descuento = from d in db.descuentos_aplicados select d;
                    Response = serializer.Serialize(descuento);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ListadoSolicitudCreditoNoAprobado()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var data = from d in db.credito_solicitud where d.operado_por != null select d;
                    Response = serializer.Serialize(data);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string LstadoSolicitudCreditoPorCliente(int cliente)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var data = from d in db.credito_solicitud where d.cliente == cliente select new
                    {
                        d.id,
                        d.cliente,
                        d.descripcion,
                        fecha = d.fecha.ToString(),
                        d.fin,
                        d.inicio,
                        d.monto,
                        d.observacion,
                        d.operado_por,
                        d.string_tipo,
                        d.tipo_solicitud
                    };
                    Response = serializer.Serialize(data);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        #endregion

        #region Util

        public string AccesosPorUsuario(string Sistema, string Vista, string Usuario)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.AccesosPorUsuario(Sistema, Vista, Usuario);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string Autenticar(string Usuario, string Clave)
        {
            int returnval = 0;

            using (ERPDataModelDataContext db = new ERPDataModelDataContext())
            {
                returnval = db.Autenticar(Usuario, Clave);
            }

            return returnval.ToString();
        }

        public string ValidarAcceso(string Sistema, string Vista, string Recurso, string Usuario)
        {
            int returnval = 0;

            using (ERPDataModelDataContext db = new ERPDataModelDataContext())
            {
                returnval = db.ValidarAcceso(Sistema, Vista, Recurso, Usuario);
            }

            return returnval.ToString();
        }

        public string VerMensajesBienvenida(int id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.VerMensajes_bienvenida(id);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        #endregion

        #region Get

        public string VerDetalleCategoria(int id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.VerDetalleCategoria(id);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string VerDocumentoDetalle(int id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.VerDocumento_detalle(id);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string VerEstadoInventario(int proveedor, int linea, string estilo, int color, int talla, int bodega_origen, int bodega_destino, string cod)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.VerEstadoInventario(proveedor, linea, estilo, color, talla, bodega_origen, bodega_destino, cod);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerDetalleDeZonas()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var zonas = from zona in db.zonas select zona;
                    List<Zona> listaDeZonas = new List<Zona>();

                    foreach (var zona in zonas)
                    {
                        Zona nuevaZona = new Zona();
                        nuevaZona.Id = zona.id;
                        nuevaZona.Nombre = zona.nombre;

                        var municipios = from m in db.municipio
                                         join mpz in db.municipiosPorZona on m.id equals mpz.municipio
                                         join d in db.departamento on m.id_departamento equals d.id
                                         where mpz.zona == nuevaZona.Id
                                         select new Municipio { Id = m.id, Nombre = m.nombre, Departamento = d.nombre };

                        foreach (var municipio in municipios)
                        {
                            nuevaZona.agregarMunicipio(municipio);
                        }

                        var clientes = from c in db.cliente
                                       join z in db.zonas on c.zona equals z.id
                                       where z.id == nuevaZona.Id
                                       select c;

                        foreach (var cliente in clientes)
                        {
                            nuevaZona.agregarCliente(cliente);
                        }

                        listaDeZonas.Add(nuevaZona);
                    }

                    Response = serializer.Serialize(listaDeZonas);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerSeries()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var series = from serie in db.serie select serie;
                    Response = serializer.Serialize(series);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerCajas()
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var cajas = from c in db.caja select c;
                    Response = serializer.Serialize(cajas);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerCaja(int id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var cajas = from c in db.caja where c.id == id select c;
                    Response = serializer.Serialize(cajas);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string CargarPedido(int idPedido)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var pedido = from p in db.factura where p.id_factura == idPedido select p;
                    Response = serializer.Serialize(pedido);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ValidarCaja(string usuario)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var cajas = from c in db.caja where c.encargado.Equals(usuario) select c;
                    Response = serializer.Serialize(cajas);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerInformacionDelCliente(int id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var cliente = from c in db.cliente where c.codigo_afiliado == id select c;
                    Response = serializer.Serialize(cliente);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string CargarStock(string pestilo, int plinea, decimal? ptalla, int? pcolor, int? pbodega, int? idcolor)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var stock = db.ConsultarStock(pestilo, plinea, pcolor, ptalla, pbodega,idcolor);

                    Response = serializer.Serialize(stock);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerDetallePedido(int idPedido)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var detalle = from d in db.detalle_factura where d.id_factura.Equals(idPedido) select d;
                    Response = serializer.Serialize(detalle);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerBanco(int idBanco)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var detalle = from d in db.bancos where d.id.Equals(idBanco) select d;
                    Response = serializer.Serialize(detalle);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerInformacionCajaCobros(string usuario)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var detalle = from d in db.caja
                                  join s in db.serie on d.serie_recibo equals s.id 
                                  where d.encargado.Equals(usuario.Trim()) select d;
                    Response = serializer.Serialize(detalle);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerCobrosPendientes(int cliente)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();
            string format = "dd/mm/yyyy";
            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var detalle = from d in db.ccdiah
                                  where d.codcli == cliente && d.saldo > 0
                                  select new {
                                      d.codcli
                                      , vence = d.vence.ToString()
                                      , fedoc = d.fedoc.ToString()
                                      ,d.caja
                                      ,d.nodoc
                                      ,d.serie
                                      ,d.concepto
                                      ,d.monto
                                      ,d.saldo
                                      ,d.abonos
                                      ,d.cargos
                                  };

                    Response = serializer.Serialize(detalle);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ObtenerMontoCobrosPendientes(int cliente)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var detalle = from d in db.ccdiah
                                  where d.codcli == cliente && d.saldo > 0
                                  select d;

                    decimal? sum = 0;

                    foreach(var d in detalle)
                    {
                        sum += d.saldo;
                    }

                    Response = serializer.Serialize(new { montoPendiente = sum });
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        #endregion

        #region Update

        public string InsertarProveedor(string Nombre, string Direccion, string Telefono, string Fax, string Correo, string NombreContacto, string TelefonoContacto, string CorreoContacto, int DiasCredito, int DiasEntrega, string PaisOrigen, string Nacionalidad, string Nit, string Registro, decimal MargenUsual, decimal DescuentoAplicable)
        {
            using (ERPDataModelDataContext db = new ERPDataModelDataContext())
            {
                db.InsertarProveedor(Nombre, Direccion, Telefono, Fax, Correo, NombreContacto, TelefonoContacto, CorreoContacto, DiasCredito, DiasEntrega, PaisOrigen, Nacionalidad, Nit, Registro, MargenUsual, DescuentoAplicable);
            }

            return "";
        }

        public string EliminarDetalleCategoria(int id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.EliminarCategoriaEspecifica(id);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string InsertarDetalleCategoria(int idCategoria, string descripcion, string usuario)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.InsertarCategoriaEspecifica(idCategoria, descripcion, usuario);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string InsertarBodega(string nombre, int encargado, string descripcion, string tiene_stock, int reutilizar_id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.InsertarBodega(nombre, encargado, descripcion, tiene_stock, reutilizar_id);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string InsertarCatalogo(string descripcion, DateTime inicio, DateTime final, string nombre, int paginas)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.InsertarCatalogo(descripcion, inicio, final, nombre, paginas);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string InsertarDocumento(string propietario)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.InsertarDocumento(propietario);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string InsertarDocumentoDetalle(int id_documento, string estilo, string codigo_origen, string descripcion, int proveedor, int catalogo, int n_pagina, int minimo_stock, decimal corrida_a, decimal corrida_b, decimal fraccion_corrida, string categorias_especificas, string observacion, string nota)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.InsertarDocumentoDetalle(id_documento, estilo, codigo_origen, descripcion, proveedor, catalogo, n_pagina, minimo_stock, corrida_a, corrida_b, fraccion_corrida, categorias_especificas, observacion, nota);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string AplicarDocumento(int id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.AplicarDocumento(id);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string InsertarTraslado(int proveedor_origen, int proveedor_nacional, int bodega_origen, int bodega_destino, string concepto, string transaccion, string usuario, int cliente, int referencia_retaceo)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.InsertarTraslado(proveedor_origen, proveedor_nacional, bodega_origen, bodega_destino, concepto, transaccion, 0, usuario, cliente, referencia_retaceo);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string InsertarDetalleTraslado(string estilo, int linea, decimal talla, int color, int id_ref, int cantidad, decimal costo)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.InsertarTrasladoDetalle(estilo, linea, talla, color, id_ref, cantidad, costo);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string EliminarDetalleTraslado(int id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.EliminarTrasladoDetalle(id);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ProcesarTraslado(int id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.ProcesarTraslado(id);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string InsertarSerie(DateTime fechaResolucion, string tipo, string serie, string descripcion, string resolucion, int del, int al)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.InsertarSerie(fechaResolucion, tipo, serie, descripcion, resolucion, del, al, 0, 0, 0);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string InsertarCaja(int id, string nombre, string encargado, int bodega_por_defecto, int serie_factura, int serie_nota_credito, int serie_recibo, int serie_ticket, short p_cambio_bodega, int serie_credito_fiscal, int serie_nota_debito, int serie_nota_remision)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.InsertarCaja(id, nombre, encargado, bodega_por_defecto, serie_factura, serie_nota_credito, serie_recibo, serie_ticket, p_cambio_bodega, null, null, null, null, serie_credito_fiscal, null, serie_nota_debito, null, serie_nota_remision, null);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string GenerarPedido(int idCliente, string concepto, int idCaja)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    int? idPedido = 0;

                    db.InsertarPedidoCabecera(idCliente, "PENDIENTE", concepto, idCaja, ref idPedido);
                    Response = serializer.Serialize(new { id_pedido = idPedido });
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string InsertarDetallePedido(int id_factura, int cantidad, int bodega, int linea, string estilo, int color, decimal talla)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    int result = db.InsertarPedidoDetalle(id_factura,cantidad,bodega,linea,estilo,color,talla);
                    if (result == 0)
                    {
                        Response = serializer.Serialize(new { error = false, message = "" });
                    }
                    else
                        Response = serializer.Serialize(new { error = true, message = "Error inesperado!" });
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string EliminarDetallePedido(int id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.EliminarPedidoDetalle(id);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string ReservarPedido (int id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.ReservarPedido(id);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string Facturar(int id_factura, int tipo_pago, bool credito_fiscal, int? id_boleta_pago,decimal? monto_en_tarjeta, decimal? monto_en_efectivo, decimal? monto_por_deposito, decimal? monto_por_cheque, decimal? monto_credito)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.Facturar(id_factura,tipo_pago,credito_fiscal,id_boleta_pago,monto_en_tarjeta,monto_en_efectivo,monto_por_deposito,monto_por_cheque,monto_credito);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string GuardarBanco(int id, string casa, string cn_codaux, string cn_cuentac, string cuenta, string nombre_cuenta, string cuenta_contable)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.InsertarBanco(id, casa, cn_codaux, cn_cuentac, cuenta, nombre_cuenta, cuenta_contable);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }
        
        public string GuardarDescuento(int tipo, decimal monto, string concepto, int cliente)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.InsertarDescuento(tipo, monto, concepto, cliente);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string EliminarDescuento(int id)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.EliminarDescuento(id);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string IngresarSolicitudCredito(int cliente, int tipo_solicitud, decimal monto, string descripcion, string observacion)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.InsertarSolicitudCredito(cliente, tipo_solicitud, monto, descripcion, observacion);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        public string AplicarAbono(int caja, int nodocumento, int cliente, decimal abono, string concepto)
        {
            string Response = "";
            var serializer = new JavaScriptSerializer();

            try
            {
                using (ERPDataModelDataContext db = new ERPDataModelDataContext())
                {
                    var Results = db.AplicarAbono(caja, nodocumento,cliente,abono,concepto);
                    Response = serializer.Serialize(Results);
                }
            }
            catch (Exception e)
            {
                Response = serializer.Serialize(new { error = true, message = e.Message });
            }

            return Response;
        }

        #endregion
    }
}