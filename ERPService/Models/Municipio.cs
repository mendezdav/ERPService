﻿using Microsoft.ServiceModel.Samples;

namespace ERPService.Models
{
    class Municipio
    {
        public int Id
        {
            get; set;
        }

        public string Nombre
        {
            get; set;
        }

        public string Departamento
        {
            get; set;
        }
    }
}
