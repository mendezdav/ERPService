﻿using Microsoft.ServiceModel.Samples;

namespace ERPService.Models
{
    class Cliente : cliente
    {
        private int codigo;
        private new string primer_nombre;
        private new string segundo_nombre;
        private new string primer_apellido;
        private new string segundo_apellido;

        public int Codigo
        {
            get
            {
                return codigo;
            }
            set
            {
                codigo = value;
            }
        }

        public string NombreCompleto
        {
            get
            {
                return primer_nombre + " " + segundo_nombre + " " + primer_apellido + " " + segundo_apellido;
            }
        }

        public string PrimerNombre
        {
            set
            {
                primer_nombre = value;
            }
        }
        public string SegundoNombre
        {
            set
            {
                segundo_nombre = value;
            }
        }

        public string PrimerApellido
        {
            set
            {
                primer_apellido = value;
            }
        }

        public string SegundoApellido
        {
            set
            {
                segundo_apellido = value;
            }
        }

    }
}
