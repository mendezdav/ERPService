﻿using System.Collections.Generic;
using Microsoft.ServiceModel.Samples;
using System.Linq;

namespace ERPService.Models
{
    class Zona : zonas
    {
        private new int id;
        private new string nombre;
        private List<Municipio> municipios;
        private List<Cliente> clientes;

        public Zona()
        {
            municipios = new List<Municipio>();
            clientes = new List<Cliente>();
        }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }

        public List<Municipio> Municipios
        {
            get
            {
                return municipios;
            }
        }

        public List<Cliente> Clientes
        {
            get
            {
                return clientes;
            }
        }

        public void agregarMunicipio(Municipio nuevoMunicipio)
        {
            municipios.Add(nuevoMunicipio);
        }

        public void agregarCliente(cliente nuevoCliente)
        {
            Cliente c = new Cliente();

            c.Codigo = nuevoCliente.codigo_afiliado;
            c.PrimerNombre = nuevoCliente.primer_nombre;
            c.SegundoNombre = nuevoCliente.segundo_nombre;
            c.PrimerApellido = nuevoCliente.primer_apellido;
            c.SegundoApellido = nuevoCliente.segundo_apellido;

            clientes.Add(c);
        }
    }
}
