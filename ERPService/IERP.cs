﻿using System;
using System.ServiceModel;

namespace Microsoft.ServiceModel.Samples
{
    [ServiceContract]
    public interface IERP
    {
        #region List

        [OperationContract]
        string ObtenerListadoProveedores();

        [OperationContract]
        string ListadoPaises();

        [OperationContract]
        string VerCategorias();

        [OperationContract]
        string ObtenerBodegas(int tipo_vista);

        [OperationContract]
        string ObtenerEmpleados();

        [OperationContract]
        string ListadoCatalogos();

        [OperationContract]
        string ObtenerDocumentosSinAplicar(string username);

        [OperationContract]
        string ObtenerTipoTransacciones();

        [OperationContract]
        string ObtenerHistorialTransacciones();

        [OperationContract]
        string VerTraslado(int id);

        [OperationContract]
        string VerTrasladoDetalle(int id);

        [OperationContract]
        string CategoriasDeSeries();

        [OperationContract]
        string ListadoBancos();

        [OperationContract]
        string ListadoTipoSolicitudCredito();

        [OperationContract]
        string ListadoTipoDescuento();

        [OperationContract]
        string ListadoDescuentos();

        [OperationContract]
        string ListadoDescuentosAplicados();

        [OperationContract]
        string ListadoSolicitudCreditoNoAprobado();

        [OperationContract]
        string LstadoSolicitudCreditoPorCliente(int cliente);

        #endregion

        #region Util

        [OperationContract]
        string AccesosPorUsuario(string Sistema, string Vista, string Usuario);

        [OperationContract]
        string Autenticar(string Usuario, string Clave);

        [OperationContract]
        string ValidarAcceso(string Sistema, string Vista, string Recurso, string Usuario);

        [OperationContract]
        string VerMensajesBienvenida(int id);

        #endregion

        #region Get

        [OperationContract]
        string VerDetalleCategoria(int id);

        [OperationContract]
        string VerDocumentoDetalle(int id);

        [OperationContract]
        string VerEstadoInventario(int proveedor, int linea, string estilo, int color, int talla, int bodega_origen, int bodega_destino, string cod);

        [OperationContract]
        string ObtenerDetalleDeZonas();

        [OperationContract]
        string ObtenerSeries();

        [OperationContract]
        string ObtenerCajas();

        [OperationContract]
        string ObtenerCaja(int id);

        [OperationContract]
        string CargarPedido(int idPedido);

        [OperationContract]
        string ValidarCaja(string usuario);

        [OperationContract]
        string ObtenerInformacionDelCliente(int id);

        [OperationContract]
        string CargarStock(string estilo, int linea, decimal? talla, int? color, int? bodega, int? idcolor);

        [OperationContract]
        string ObtenerDetallePedido(int idPedido);

        [OperationContract]
        string ObtenerBanco(int idBanco);

        [OperationContract]
        string ObtenerInformacionCajaCobros(string usuario);

        [OperationContract]
        string ObtenerCobrosPendientes(int cliente);

        [OperationContract]
        string ObtenerMontoCobrosPendientes(int cliente);

        #endregion

        #region Update

        [OperationContract]
        string InsertarProveedor(string Nombre, string Direccion, string Telefono, string Fax, string Correo, string NombreContacto, string TelefonoContacto, string CorreoContacto, int DiasCredito, int DiasEntrega, string PaisOrigen, string Nacionalidad, string Nit, string Registro, decimal MargenUsual, decimal DescuentoAplicable);

        [OperationContract]
        string EliminarDetalleCategoria(int id);

        [OperationContract]
        string InsertarDetalleCategoria(int idCategoria, string descripcion, string usuario);

        [OperationContract]
        string InsertarBodega(string nombre, int encargado, string descripcion, string tiene_stock, int reutilizar_id);

        [OperationContract]
        string InsertarCatalogo(string descripcion, DateTime inicio, DateTime final, string nombre, int paginas);

        [OperationContract]
        string InsertarDocumento(string propietario);

        [OperationContract]
        string InsertarDocumentoDetalle(int id_documento, string estilo, string codigo_origen, string descripcion, int proveedor, int catalogo, int n_pagina, int minimo_stock, decimal corrida_a, decimal corrida_b, decimal fraccion_corrida, string categorias_especificas, string observacion, string nota);

        [OperationContract]
        string AplicarDocumento(int id);

        [OperationContract]
        string InsertarTraslado(int proveedor_origen, int proveedor_nacional, int bodega_origen, int bodega_destino, string concepto, string transaccion, string usuario, int cliente, int referencia_retaceo);

        [OperationContract]
        string InsertarDetalleTraslado(string estilo, int linea, decimal talla, int color, int id_ref, int cantidad, decimal costo);

        [OperationContract]
        string EliminarDetalleTraslado(int id);

        [OperationContract]
        string ProcesarTraslado(int id);

        [OperationContract]
        string InsertarSerie(DateTime fechaResolucion, string tipo, string serie, string descripcion, string resolucion, int del, int al);

        [OperationContract]
        string InsertarCaja(int id, string nombre, string encargado, int bodega_por_defecto, int serie_factura, int serie_nota_credito, int serie_recibo, int serie_ticket, short p_cambio_bodega, int serie_credito_fiscal, int serie_nota_debito, int serie_nota_remision);

        [OperationContract]
        string GenerarPedido(int idCliente, string concepto, int idCaja);

        [OperationContract]
        string InsertarDetallePedido(int id_factura, int cantidad, int bodega, int linea, string estilo, int color, decimal talla);

        [OperationContract]
        string EliminarDetallePedido(int id);

        [OperationContract]
        string ReservarPedido(int id);

        [OperationContract]
        string Facturar(int id_factura, int tipo_pago, bool credito_fiscal, int? id_boleta_pago, decimal? monto_en_tarjeta, decimal? monto_en_efectivo, decimal? monto_por_deposito, decimal? monto_por_cheque, decimal? monto_credito);

        [OperationContract]
        string GuardarBanco(int id, string casa, string cn_codaux, string cn_cuentac, string cuenta, string nombre_cuenta, string cuenta_contable);

        [OperationContract]
        string GuardarDescuento(int tipo, decimal monto, string concepto, int cliente);

        [OperationContract]
        string EliminarDescuento(int id);

        [OperationContract]
        string IngresarSolicitudCredito(int cliente, int tipo_solicitud, decimal monto, string descripcion, string observacion);

        [OperationContract]
        string AplicarAbono(int caja, int nodocumento, int cliente, decimal abono, string concepto);

        #endregion
    }
}
